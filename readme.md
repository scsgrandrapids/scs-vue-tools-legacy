scs-vue-tools
==============

<h3>SCS Vue Tools for Reactive Laravel Interfaces</h3>

Step 1:

Set up composer, add the package to your require tag:
```
composer require scs/scs-vue-tools
```

run
```
composer install
```

Step 2:

Add service provider to config/app.php
```
Scs\VueTools\VueToolsServiceProvider::class,
```

Step 3:

We need to publish the required files and install correct bower dependencies

Run:
```
php artisan vendor:publish
bower install bootstrap#~3.3.6 moment#~2.11.2 select2#~4.0.1 icheck#~1.0.2 https://github.com/vuejs/vue.git#~1.0.21 --save
```

Step 4:

Add necessary files to the main view layout that will be using Vue.js

Include the vue layout in the footer, after other JS has been included but before page scripts are yielded
```
@include('vuetools::vue')
```

Don't forget to add the CSS and JS assets to the main view layout:
```
<script src="{{ asset('bower_components/vue/dist/vue.js') }}"></script>
<script src="{{ asset('bower_components/vue-router/dist/vue-router.js') }}"></script>
```