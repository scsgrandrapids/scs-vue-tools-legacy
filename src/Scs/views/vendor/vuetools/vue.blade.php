@include('vuetools::vue_master')

@yield('vue_scripts')

@yield('vue_mixins')

@yield('vue_components')
