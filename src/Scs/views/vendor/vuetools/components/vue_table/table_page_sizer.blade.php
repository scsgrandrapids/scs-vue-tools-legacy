<template id="vue-table-page-sizer">
	<div class="input-group">
		<strong class="input-group-addon" style="border: none; background-color: transparent; padding-left: 8px;">
			Page Size:
		</strong>
	    <select v-model="comPageSize"
	    	class="form-control"
		>
	    	<option v-for="size in sizes" :value="size">
	            @{{ size }}
	        </option>
	    </select>
	</div>
</template>

@section('vue_components')
<script>
	vueComponents.tablePageSizer = Vue.extend({
		template: '#vue-table-page-sizer',
		props: {
			sizes: {
				type: Array,
	      		default: function () {
	        		return [10, 25, 50, 100];
	      		}
			}
		},
		computed: {
			comPageSize: {
				get: function() {
					return this.$parent.filterArgs.limitBy.pageSize;
				},
				set: function(val) {
					this.$parent.filterArgs.limitBy.pageSize = val;
				}
			}
		}
	});

	Vue.component('table-page-sizer', vueComponents.tablePageSizer);

</script>
@append