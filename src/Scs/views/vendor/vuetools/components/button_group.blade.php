<template id="vue-button-group">
	<div class="btn-group vue-button-group">
		<button v-for='button in buttons' 
	    	:style="(value == button.value) ? 'border: 1px solid #2a88bd;' : ''"
	    	type="button" 
	    	tabindex="-1" 
	    	:class="{
	    		'btn': true, 
	    		'btn-xs': (size == 'small'), 
	    		'btn-lg': (size == 'large'), 
	    		'btn-default': (value != button.value), 
	    		'btn-primary': (value == button.value)
    		}"
	    	@click="updateValue(button.value)"
    	>
    		@{{{ button.text }}}
    	</button>
    	<input type="text" 
			style="height: 0; width: 0; border: none; padding: 0; margin: 0; float: left;" 
			@keyup="moveFocus"
			:tabindex="tabindex"
		/>
	</div>
</template>

@section('vue_components')
<script>
	vueComponents.buttonGroup = Vue.extend({
		template: '#vue-button-group',
		props: {
			buttons: Array,
			value: null,
			size: {
				type: String,
				default: 'small'
			},
			tabindex: {
				type: Number,
				default: 0
			}
		},
		methods: {
			updateValue: function(val) {
				this.value = val;
			},
			moveFocus: function(event) {
				var vm = this;
				var index = this.buttons.findIndex(function(button) {
					return (button.value == vm.value);
				});
				
				if(event.keyCode == 37) {
					if(index > 0) {
						this.$set('value', this.buttons[index - 1].value);
					}
				} else if(event.keyCode == 39) {
					if(index < (this.buttons.length - 1)) {
						this.$set('value', this.buttons[index + 1].value);
					}
				}
			}
		}
	});

	Vue.component('button-group', vueComponents.buttonGroup);

</script>
@append