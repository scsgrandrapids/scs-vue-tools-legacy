<?php

return [

	/* this contains useful defaults that are used by the vue tools framework */

	'databaseDateTimeFormat' => env('DB_DATETIME_FORMAT', 'YYYY-MM-DD HH:mm:ss'),
	'defaultDateFormat' => env('DEFAULT_DATE_FORMAT', 'MM/DD/YY'),
	'defaultTimeFormat' => env('DEFAULT_TIME_FORMAT', 'h:mm a'),
	'defaultDecimals' => env('DEFAULT_DECIMALS', 2),
	'defaultDecimalPoint' => env('DEFAULT_DECIMAL_POINT', '.'),
	'defaultThousandSeparator' => env('DEFAULT_THOUSAND_SEPERATOR', ','),

	'defaultDatePickerFormat' => env('DEFAULT_DATEPICKER_FORMAT', 'mm/dd/yy'),
];