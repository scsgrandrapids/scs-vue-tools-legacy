<?php 

namespace Scs\VueTools;

use Illuminate\Support\ServiceProvider;

class VueToolsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
		$this->loadViewsFrom(__DIR__.'/../views/vendor/vuetools/', 'vuetools');
		
		// Publish the config and views/vue files
		$this->publishes([
				__DIR__.'/../config/vue-tools.php' => config_path('vue-tools.php'),
				__DIR__.'/../views/vendor/vuetools/' => base_path('resources/views/vendor/vuetools'),
				__DIR__.'/../.bowerrc' => base_path('.bowerrc'),
				__DIR__.'/../bower.json' => base_path('bower.json')
		], 'vue');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfigFrom(
		    __DIR__.'/../config/vue-tools.php', 'vue-tools'
		);
	}

}
